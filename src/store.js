import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    appTitle: null,
    token: null,
    user: {},
  },
  mutations: {
    updatedToken(state, token) {
      // eslint-disable-next-line no-param-reassign
      state.token = token;
    },
    updatedUser(state, user) {
      // eslint-disable-next-line no-param-reassign
      state.user = user;
    },
    updatedTitle(state, tittle) {
      // eslint-disable-next-line no-param-reassign
      state.appTitle = tittle;
    },
  },
  actions: {

  },
  getters: {
    isAuthenticated: state => !!state.token,
    getToken: state => state.token,
    getUser: state => state.user,
    isAdmin: state => !!state.user.admin,
    getTitle: state => state.appTitle,
  },
});
