import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/account/Login.vue';
import Aluno from './views/alunos/Index.vue';
import Escola from './views/escolas/Index.vue';
import Usuario from './views/usuarios/Index.vue';
import Dashboard from './views/dashboard/Index.vue';
import Carteirinha from './views/dashboard/Carteirinha.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: Login,
      meta: {
        guest: true,
      },
    },
    {
      path: '/',
      component: Dashboard,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/dashboard/carteirinha',
      component: Carteirinha,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/alunos',
      component: Aluno,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/escolas',
      component: Escola,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/usuarios',
      component: Usuario,
      meta: {
        requiresAuth: true,
        is_admin: true,
      },
    },
    {
      path: '*',
      name: '/login',
      component: Login,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    console.log('primeiro if');
    if (localStorage.getItem('token') == null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
    /**
    else if (to.matched.some(record => record.meta.is_admin)) {
      const user = JSON.parse(localStorage.getItem('usuario'));
      if (to.matched.some(record => record.meta.is_admin)) {
        if (user.isAdmin === 1) {
          next();
        } else {
          next({ name: '/dashboard' });
        }
      }
    }
      */
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem('token') == null) {
      next();
    } else {
      next({ name: '/dashboard' });
    }
  } else if (localStorage.getItem('token') == null) {
    next();
  } else {
    next({ name: '/dashboard' });
  }
});

export default router;
