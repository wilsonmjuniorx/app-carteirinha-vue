import { http } from './config';

export default {
  listar: () => http.get('escolas'),
  salvar: escola => http.post('escola', escola),
  alterar: escola => http.put('escola', escola),
  deletar: escola => http.delete('escola', { data: escola }),
};
