import { http } from './config';

export default {
  login: usuario => http.post('login', usuario),
};
