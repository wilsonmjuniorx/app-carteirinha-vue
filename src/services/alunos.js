import { http } from './config';

export default {
  listar: () => http.get('alunos'),
  salvar: aluno => http.post('aluno', aluno),
  alterar: aluno => http.put('aluno', aluno),
  deletar: aluno => http.delete('aluno', { data: aluno }),
};
